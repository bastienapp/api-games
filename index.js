import express from 'express';
import cors from 'cors';
import path from 'path';
import { URL } from 'url';
import 'dotenv/config';
import movieList from './db.js';

const app = express();
app.use(cors())
const port = process.env.PORT || 8000;

const __dirname = new URL('.', import.meta.url).pathname;

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'));
});

app.get("/api/games", (req, res) => {
  res.send(movieList);
});

app.get("/api/games/:id", (req, res) => {
  const { id } = req.params;
  console.log(id);
  const result = movieList.find((movie) => movie.id == id);
  if (result) {
    res.send(result);
  } else {
    res.sendStatus(404);
  }
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
