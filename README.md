# Games API

## Endpoints

* `GET /api/games`
* `GET /api/games/:id`

### Request game list

`GET /api/games`

#### Response

* [/api/games](./api/games)

```
HTTP/1.1 200 OK
Status: 200 OK
Content-Type: application/json
```
```json
[
  {
    "id": 4200,
    "slug": "portal-2",
    "name": "Portal 2",
    "released": "2011-04-19",
    "background_image": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg",
    "rating": 4.6,
    "saturated_color": "0f0f0f",
    "dominant_color": "0f0f0f",
    "genres": [
      {
        "id": 2,
        "name": "Shooter",
        "slug": "shooter",
        "games_count": 21882,
        "image_background": "https://media.rawg.io/media/games/198/1988a337305e008b41d7f536ce9b73f6.jpg"
      },
      {
        "id": 7,
        "name": "Puzzle",
        "slug": "puzzle",
        "games_count": 42966,
        "image_background": "https://media.rawg.io/media/screenshots/aa7/aa7cc3831b020d2830a6e4cf7b40a73b.jpeg"
      }
    ],
    "clip": {
      "clip": "https://media.rawg.io/media/stories-640/fde/fde8aaeeab956f6b705bbb4161b09004.mp4",
      "clips": {
        "320": "https://media.rawg.io/media/stories-320/b26/b265f65b9f16dc20245863636d4094b2.mp4",
        "640": "https://media.rawg.io/media/stories-640/fde/fde8aaeeab956f6b705bbb4161b09004.mp4",
        "full": "https://media.rawg.io/media/stories/671/67196dea179367b70212bdaed88ba451.mp4"
      },
      "preview": "https://media.rawg.io/media/stories-previews/faf/faf0bb37b806db65f1c76395c8f36c7c.jpg"
    },
    "short_screenshots": [
      {
        "id": -1,
        "image": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
      },
      {
        "id": 99018,
        "image": "https://media.rawg.io/media/screenshots/221/221a03c11e5ff9f765d62f60d4b4cbf5.jpg"
      },
      {
        "id": 99019,
        "image": "https://media.rawg.io/media/screenshots/173/1737ff43c14f40294011a209b1012875.jpg"
      },
      {
        "id": 99020,
        "image": "https://media.rawg.io/media/screenshots/b11/b11a2ae0664f0e8a1ef2346f99df26e1.jpg"
      },
      {
        "id": 99021,
        "image": "https://media.rawg.io/media/screenshots/9b1/9b107a790909b31918ebe2f40547cc85.jpg"
      },
      {
        "id": 99022,
        "image": "https://media.rawg.io/media/screenshots/d05/d058fc7f7fa6128916c311eb14267fed.jpg"
      },
      {
        "id": 99023,
        "image": "https://media.rawg.io/media/screenshots/415/41543dcc12dffc8e97d85a56ad42cda8.jpg"
      }
    ]
  },
  {},
  {},
  {},
]
```

### Request game by id

`GET /api/games/:id`

#### Response (if id not found)

* [/api/games/42](./api/games/42)

```
HTTP/1.1 404 Not Found
Status: 404 Not Found
```

#### Response (if id exists)

* [/api/games/12020](./api/games/12020)

```
HTTP/1.1 200 OK
Status: 200 OK
Content-Type: application/json
```
```json
{
  "id": 12020,
  "slug": "left-4-dead-2",
  "name": "Left 4 Dead 2",
  "released": "2009-11-17",
  "background_image": "https://media.rawg.io/media/games/c25/c25ebb8eb08790277ae2e2dce0d62174.jpg",
  "rating": 4.08,
  "saturated_color": "0f0f0f",
  "dominant_color": "0f0f0f",
  "genres": [
    {
      "id": 4,
      "name": "Action",
      "slug": "action",
      "games_count": 78074,
      "image_background": "https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg"
    },
    {
      "id": 2,
      "name": "Shooter",
      "slug": "shooter",
      "games_count": 21835,
      "image_background": "https://media.rawg.io/media/games/8e0/8e032ac4faf1136e7d708bb3ac61af23.jpg"
    }
  ],
  "clip": {
    "clip": "https://media.rawg.io/media/stories-640/6bf/6bf9c27538426b187908bafd61160c3e.mp4",
    "clips": {
      "320": "https://media.rawg.io/media/stories-320/ca2/ca2a28e46c37b95cc89a1ad09ca11319.mp4",
      "640": "https://media.rawg.io/media/stories-640/6bf/6bf9c27538426b187908bafd61160c3e.mp4",
      "full": "https://media.rawg.io/media/stories/641/6410df150314b7903fb651393711847f.mp4"
    },
    "preview": "https://media.rawg.io/media/stories-previews/ecb/ecba71900f6e78ccc6521c4d24c399d6.jpg"
  },
  "short_screenshots": [
    {
      "id": -1,
      "image": "https://media.rawg.io/media/games/c25/c25ebb8eb08790277ae2e2dce0d62174.jpg"
    },
    {
      "id": 99748,
      "image": "https://media.rawg.io/media/screenshots/4c0/4c043fd1a5ff78900483f2e82580fea0.jpg"
    },
    {
      "id": 99749,
      "image": "https://media.rawg.io/media/screenshots/c90/c9071628c238fbc20b366e2413dd8b4a.jpg"
    },
    {
      "id": 99750,
      "image": "https://media.rawg.io/media/screenshots/e29/e293b0f98092b8c0dbe24d66846088bb.jpg"
    },
    {
      "id": 99751,
      "image": "https://media.rawg.io/media/screenshots/168/16867bc76b385eb0fb749e41f7ada93d.jpg"
    },
    {
      "id": 99752,
      "image": "https://media.rawg.io/media/screenshots/fb9/fb917e562f311f48ff8d27632bd29a80.jpg"
    },
    {
      "id": 99753,
      "image": "https://media.rawg.io/media/screenshots/5f2/5f2ca569912add2a211b20ba3f576b97.jpg"
    }
  ]
}
```
