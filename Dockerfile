# install LTS node
FROM node:lts-slim as build

# set app directory
WORKDIR /usr/src/app

# install app dependencies
# this is done before the following COPY command to take advantage of layer caching
COPY package.json .
COPY package-lock.json .
RUN npm ci

# copy app source to destination container
COPY . .

# expose container port
# EXPOSE 8000

CMD ["npm", "start"]